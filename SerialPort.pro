#-------------------------------------------------
#
# Project created by QtCreator 2012-03-31T17:21:34
#
#-------------------------------------------------

QT       += core gui widgets serialport
CONFIG  += thread

TARGET = SerialPort
TEMPLATE = app

CONFIG(release, debug|release){
    DEFINES += QT_NO_DEBUG_OUTPUT
}

INCLUDEPATH += src

SOURCES += \
    src/mainwindow.cpp \
    src/linecurve.cpp \
    src/main.cpp

HEADERS  += \
    src/linecurve.h \
    src/mainwindow.h

FORMS    += \
    src/mainwindow.ui

RESOURCES += res/SerialPort.qrc

win32 {
    LIBS += -lsetupapi -luuid -ladvapi32
    RC_FILE = res/myapp.rc
}
unix:!macx {
    LIBS += -ludev
}

#qxt
INCLUDEPATH += lib/qxt
include(lib/qxt/qxt.pri)

#qwt
CONFIG(release, debug|release){
    LIBS += -lqwt
} else {
    LIBS += -lqwtd
}

