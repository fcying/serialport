#include <QApplication>
#include <QDesktopWidget>
#include "mainwindow.h"

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);

    MainWindow w;
    w.move(QApplication::desktop()->width()/2-w.width()/2,
           QApplication::desktop()->height()/2-w.height()/2);

    w.show();

    return  app.exec();
}
