﻿#include "mainwindow.h"
#include "ui_mainwindow.h"

#define VERSION "2016.05.31"

#define TAB_PLOT_INDEX  1

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    this->setWindowTitle(QString("%1 %2").arg("SerialPort ").arg(VERSION));

    ui->cbAutoWrap->setChecked(true);
    on_cbAutoWrap_stateChanged(Qt::Checked);

    //add qxtcheckcombobox
    this->ccbSelectCurve = new QxtCheckComboBox(ui->groupBox);
    ui->gridLayout->addWidget(this->ccbSelectCurve, 6, 1, 1, 1);

    connect(this->ccbSelectCurve, SIGNAL(checkedItemsChanged(QStringList)),
            this, SLOT(CheckedItemsChanged(QStringList)));

    ui->outputScreen->setMaximumBlockCount(1000);
    ui->inputScreen->setText("11 22 33 44 55 66 77 88 99 AA BB CC DD EE FF 00\r\n");

    this->portIsOpened = false;
    this->isPaused = false;
    this->disArrayBuf.clear();
    this->disString.clear();
#if _QWT_EN
    this->curve = NULL;
#endif
    this->pointBuf = NULL;

    readTimer = new QTimer(this);
    readTimer->setInterval(1);
    readTimer->setSingleShot(true);
    connect(readTimer, SIGNAL(timeout()), this, SLOT(DisplayData()));

    writeTimer = new QTimer(this);
    connect(writeTimer, SIGNAL(timeout()), this, SLOT(on_btnWriteData_clicked()));

    port = new QSerialPort(this);
    connect(port, SIGNAL(readyRead()), this, SLOT(readTimerStart()));
    connect(port, SIGNAL(error(QSerialPort::SerialPortError)), this, SLOT(ProcessPortError(QSerialPort::SerialPortError)));

    this->enumerateTimer = new QTimer(this);
    this->enumerateTimer->start(1000);
    connect(enumerateTimer, SIGNAL(timeout()), this, SLOT(UpdatePortName()));

    this->InitPlotCanvas();
    ui->sbSensorCount->setValue(5);
}

MainWindow::~MainWindow()
{
    DelPlotCurves();
    if (this->portIsOpened)
    {
        this->port->close();
    }
    delete this->port;
    delete ui;
}

void MainWindow::UpdatePortName()
{
    QString lastSelectText = ui->cmbPort->currentText();

    ui->cmbPort->clear();
    foreach (QSerialPortInfo info, QSerialPortInfo::availablePorts())
    {
        ui->cmbPort->addItem(info.portName());
    }

    int index = ui->cmbPort->findText(lastSelectText);
    if (index == -1)
    {
        ui->cmbPort->setCurrentIndex(ui->cmbPort->count()-1);
        if (this->portIsOpened)
        {
            this->on_btnSwitch_clicked();
        }
    }
    else
        ui->cmbPort->setCurrentIndex(index);
}

void MainWindow::ProcessPortError(QSerialPort::SerialPortError error)
{
    qDebug("%d", error);
    switch (error)
    {
        case QSerialPort::NoError:
            break;
        case QSerialPort::DeviceNotFoundError:
            ui->outputScreen->appendPlainText("SerialPort::DeviceNotFoundError");
            break;
        case QSerialPort::PermissionError:
            ui->outputScreen->appendPlainText("SerialPort::PermissionError");
            break;
        case QSerialPort::OpenError:
            ui->outputScreen->appendPlainText("SerialPort::OpenError");
            break;
        case QSerialPort::NotOpenError:
            ui->outputScreen->appendPlainText("SerialPort::NotOpenError");
            break;
        case QSerialPort::ParityError:
            ui->outputScreen->appendPlainText("SerialPort::ParityError");
            break;
        case QSerialPort::FramingError:
            ui->outputScreen->appendPlainText("SerialPort::FramingError");
            break;
        case QSerialPort::BreakConditionError:
            ui->outputScreen->appendPlainText("SerialPort::BreakConditionError");
            break;
        case QSerialPort::WriteError:
            ui->outputScreen->appendPlainText("SerialPort::WriteError");
            break;
        case QSerialPort::ReadError:
            ui->outputScreen->appendPlainText("SerialPort::ReadError");
            break;
        case QSerialPort::ResourceError:
            ui->outputScreen->appendPlainText("SerialPort::ResourceError");
            this->on_btnSwitch_clicked();
            break;
        case QSerialPort::UnsupportedOperationError:
            ui->outputScreen->appendPlainText("SerialPort::UnsupportedOperationError");
            break;
        case QSerialPort::TimeoutError:
            ui->outputScreen->appendPlainText("SerialPort::TimeoutError");
            break;
        case QSerialPort::UnknownError:
            ui->outputScreen->appendPlainText("SerialPort::UnknownError");
            break;
        default:
            break;
    }
}

//写串口数据
void MainWindow::on_btnWriteData_clicked()
{
    if (this->portIsOpened)
    {
        QByteArray txBuf;
        QString text = ui->inputScreen->document()->toPlainText();
        qint64 length = text.length();
        for (int i=0,j=0; ; )
        {
            i = text.indexOf('\n', j);
            if (i != -1)
            {
                if ((i==0) || (text.at(i-1)!='\r'))
                {
                    text = text.insert(i,'\r');
                    j = i+2;
                    length++;
                }
                else
                    j++;
            }
            else
                break;
        }
        if (ui->cbHex->isChecked())
        {
            bool ok;
            for (int i=0,j=0; i!=-1; j++)
            {
                i = text.indexOf(' ');
                txBuf[j] = text.left(i).toInt(&ok, 16);
                text = text.remove(0,i+1);
                length = j+1;
            }
        }
        else
        {
            for (int i=0; i<text.length(); i++)
            {
                txBuf[i] = text.at(i).toLatin1();
            }
        }
        if (port->write(txBuf) < 0)
        {
            ui->outputScreen->appendPlainText("write error");
        }
    }
    else
    {
        ui->outputScreen->appendPlainText(tr("串口未打开"));
        ui->cbAutoWrite->setChecked(false);
    }
}

void MainWindow::readTimerStart()
{
    this->readTimer->start();
}

//打开关闭串口
void MainWindow::on_btnSwitch_clicked()
{
    if (this->portIsOpened)
    {
        port->close();
        ui->btnSwitch->setText(tr("打开"));
        this->portIsOpened = false;
    }
    else
    {
        port->setPortName(ui->cmbPort->currentText());
        port->setBaudRate(ui->cmbBaudRate->currentText().toInt());
        port->setDataBits(QSerialPort::DataBits(ui->cmbDataBits->currentText().toInt()));

        if (ui->cmbParity->currentText() == "None")
            port->setParity(QSerialPort::NoParity);
        else if (ui->cmbParity->currentText() == "Odd")
            port->setParity(QSerialPort::NoParity);
        else if (ui->cmbParity->currentText() == "Even")
            port->setParity(QSerialPort::EvenParity);
        else if (ui->cmbParity->currentText() == "Mark")
            port->setParity(QSerialPort::MarkParity);
        else if (ui->cmbParity->currentText() == "Space")
            port->setParity(QSerialPort::SpaceParity);

        if (ui->cmbStopBits->currentText() == "1")
            port->setStopBits(QSerialPort::OneStop);
        else if (ui->cmbStopBits->currentText() == "1.5")
            port->setStopBits(QSerialPort::OneAndHalfStop);
        else if (ui->cmbStopBits->currentText() == "2")
            port->setStopBits(QSerialPort::TwoStop);

        if (port->open(QIODevice::ReadWrite))
        {
            ui->outputScreen->appendPlainText(
                        ui->cmbPort->currentText() + " open success");
            ui->btnSwitch->setText(tr("关闭"));
            this->portIsOpened = true;
        }
        else
        {
            ui->outputScreen->appendPlainText("open false");
        }
    }
}

// 显示串口数据
void MainWindow::DisplayData()
{
    if (port->isOpen())
    {
        disArrayBuf.append(port->readAll());
    }
    else
    {
        this->on_btnSwitch_clicked();
        return;
    }

    if (ui->cbHex->isChecked())
    {
        for (int i=0; i<disArrayBuf.length(); ++i)
        {
            disString.append(QString().sprintf("%02X ",
                                (quint8)disArrayBuf.at(i)));
        }
        disArrayBuf.clear();
    }
    else
    {
        if (disArrayBuf.endsWith('\r'))
        {
            disString.append(disArrayBuf.left(disArrayBuf.length()-1));
            disArrayBuf.remove(0, disArrayBuf.length()-1);
        }
        else
        {
            disString.append(disArrayBuf);
            disArrayBuf.clear();
        }
    }

    if (!this->isPaused)
    {
        if (ui->tabWidget->currentIndex() == 0) //显示数据
        {
            ui->outputScreen->moveCursor(QTextCursor::End);
            ui->outputScreen->insertPlainText(disString);
            ui->outputScreen->moveCursor(QTextCursor::End);
            disString.clear();
        }
        else if (ui->tabWidget->currentIndex() == 1)    //显示波形
        {
            QString oneRawdata; //one rawdata end with "\n"
            qint64 oneData;
            qint64 rawdataLineIndex = 0;
            qint64 oneDataIndex = 0;
            qint32 i = 0;
            bool ok;
            while (1) //把数据放进pointBuf
            {
                rawdataLineIndex = disString.indexOf("\n");
                if (rawdataLineIndex == -1)
                    break;
                oneRawdata = disString.left(++rawdataLineIndex);
                disString = disString.remove(0, rawdataLineIndex);
                this->dataCount++;
                for (i=0; i<this->sensorMaxDisCount; i++)
                {
                    oneDataIndex = oneRawdata.indexOf(" ");
                    if (oneDataIndex == -1)
                        break;
                    oneData = oneRawdata.left(oneDataIndex).toLong(&ok, 10);
                    oneRawdata = oneRawdata.remove(0, ++oneDataIndex);
                    if (ok == false)
                        continue;
                    this->pointBuf[i].append(QPointF(dataCount, oneData));
                }
                this->realSensorCount = i;
            }
            this->RePlot();
        }
    }
}

//plot 重绘
void MainWindow::RePlot()
{
#if _QWT_EN
    for (qint32 i=0; i<this->realSensorCount; i++)
    {
        if (i >= this->sensorMaxDisCount)
            break;
        curve[i].setSamples(pointBuf[i]);
    }
    this->qwtPlot->setAxisScale(QwtPlot::xBottom, 0, this->dataCount);
    this->qwtPlot->replot();
#endif
}

//初始化plot
void MainWindow::InitPlotCanvas()
{
#if _QWT_EN
    this->qwtPlot = new QwtPlot(ui->tabPlot);
    QVBoxLayout *plotLayout = new QVBoxLayout(ui->tabPlot);
    plotLayout->addWidget(this->qwtPlot);

    new QwtPlotMagnifier(this->qwtPlot->canvas());
    new QwtPlotZoomer(this->qwtPlot->canvas());

    this->qwtPlot->setAxisLabelAlignment(QwtPlot::xBottom, Qt::AlignBottom|Qt::AlignLeft);
#endif
}

void MainWindow::InitPlotCurves()
{
#if _QWT_EN
    //初始化波形图数组
    this->sensorMaxDisCount = ui->sbSensorCount->text().toInt();
    this->pointBuf = new QVector<QPointF>[this->sensorMaxDisCount];
    this->curve = new QwtPlotCurve[this->sensorMaxDisCount];

    for (qint32 i=0; i<this->sensorMaxDisCount; i++)
    {
        pointBuf[i].clear();
        this->curve[i].setRenderHint(QwtPlotItem::RenderAntialiased);
        //this->curve[i].setLegendAttribute(QwtPlotCurve::LegendShowLine, true);
        switch(i%10)
        {
            case 0:
            this->curve[i].setPen(QPen(Qt::black));
                break;
            case 1:
            this->curve[i].setPen(QPen(Qt::red));
                break;
            case 2:
            this->curve[i].setPen(QPen(Qt::blue));
                break;
            case 3:
            this->curve[i].setPen(QPen(Qt::green));
                break;
            case 4:
            this->curve[i].setPen(QPen(Qt::darkRed));
                break;
            case 5:
            this->curve[i].setPen(QPen(Qt::darkCyan));
                break;
            case 6:
            this->curve[i].setPen(QPen(Qt::yellow));
                break;
            case 7:
            this->curve[i].setPen(QPen(Qt::darkGray));
                break;
            case 8:
            this->curve[i].setPen(QPen(Qt::darkGreen));
                break;
            case 9:
            this->curve[i].setPen(QPen(Qt::magenta));
                break;
            case 10:
                break;
            default:
            this->curve[i].setPen(QPen(Qt::black));
                break;
        }
        this->curve[i].attach(this->qwtPlot);
    }
#endif
}

void MainWindow::DelPlotCurves()
{
#if _QWT_EN
    if (this->curve != NULL)
    {
        delete []this->curve;
        this->curve = NULL;
    }
    if (this->pointBuf != NULL)
    {
        delete []this->pointBuf;
        this->pointBuf = NULL;
    }
#endif
}

//标签页切换
void MainWindow::on_tabWidget_currentChanged(int index)
{
    if (index == TAB_PLOT_INDEX)
    {
        this->dataCount = 0;
        DelPlotCurves();
        InitPlotCurves();
    }
    else
    {
        DelPlotCurves();
    }
}

//选择显示线条
static bool lastSelectAll = false;
void MainWindow::CheckedItemsChanged(QStringList l)
{

    disconnect(this->ccbSelectCurve,
               SIGNAL(checkedItemsChanged(QStringList)), 0, 0);

    if (l.count() != 0)
    {
        if (l.at(0) != "All")
        {
            if (lastSelectAll)
            {
                for (int i=0; i<this->ccbSelectCurve->count(); i++)
                    this->ccbSelectCurve->setItemCheckState(i, Qt::Unchecked);
            }
            lastSelectAll = false;
        }
        else
        {
            if (lastSelectAll)
            {
                this->ccbSelectCurve->setItemCheckState(0, Qt::Unchecked);
                lastSelectAll = false;
            }
            else
            {
                for (int i=0; i<this->ccbSelectCurve->count(); i++)
                    this->ccbSelectCurve->setItemCheckState(i, Qt::Checked);
                lastSelectAll = true;
            }
        }
    }
#if _QWT_EN
    if (this->curve != NULL)
    {
        for (int i=1; i<this->ccbSelectCurve->count(); i++)
        {
            if (this->ccbSelectCurve->itemCheckState(i) == Qt::Checked)
                this->curve[i-1].show();
            else
                this->curve[i-1].hide();
        }
    }
#endif

    connect(this->ccbSelectCurve, SIGNAL(checkedItemsChanged(QStringList)),
            this, SLOT(CheckedItemsChanged(QStringList)));
}

//线条数设置
void MainWindow::on_sbSensorCount_valueChanged(int arg1)
{
    disconnect(this->ccbSelectCurve,
               SIGNAL(checkedItemsChanged(QStringList)), 0, 0);

    this->ccbSelectCurve->clear();
    this->ccbSelectCurve->addItem(tr("All"));
    for (int i=0; i<arg1; ++i)
        this->ccbSelectCurve->addItem(tr("L ")+QString().number(i));

    lastSelectAll = false;

#if _QWT_EN
    if (ui->tabWidget->currentIndex() == TAB_PLOT_INDEX)
    {
        DelPlotCurves();
        InitPlotCurves();
    }
#endif

    connect(this->ccbSelectCurve, SIGNAL(checkedItemsChanged(QStringList)),
            this, SLOT(CheckedItemsChanged(QStringList)));

    this->ccbSelectCurve->setItemCheckState(0, Qt::Checked);    //select all curves


}

//边缘自动换行
void MainWindow::on_cbAutoWrap_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked)
    {
        ui->outputScreen->setLineWrapMode(QPlainTextEdit::WidgetWidth);
    }
    else
    {
        ui->outputScreen->setLineWrapMode(QPlainTextEdit::NoWrap);
    }
}

//连续写入时间
void MainWindow::on_cbAutoWrite_stateChanged(int arg1)
{
    if (arg1 == Qt::Checked)
    {
        this->writeTimer->start(ui->sbSendTime->text().toInt());
    }
    else
    {
        this->writeTimer->stop();
    }
}

//暂停
void MainWindow::on_btnPause_clicked()
{
    if (this->isPaused)
    {
        ui->btnPause->setText(tr("暂停"));
        this->isPaused = false;
        ui->outputScreen->appendPlainText("continue");
    }
    else
    {
        ui->btnPause->setText(tr("继续"));
        this->isPaused = true;
    }
}

//退出
void MainWindow::on_btnExit_clicked()
{
    this->close();
}

//清空显示
void MainWindow::on_btnClear_clicked()
{
    ui->outputScreen->clear();

    if (ui->tabWidget->currentIndex() == TAB_PLOT_INDEX)
    {
        for (qint8 i=0; i<this->sensorMaxDisCount; i++)
            this->pointBuf[i].clear();
    }
}
