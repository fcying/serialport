#include "linecurve.h"

#ifdef LINECURVE_H

LineCurve::LineCurve(QWidget *parent):
    QwtPlot(parent)
{
    this->setAxisScale(QwtPlot::xBottom,0.0,10.0);
    this->setAxisScale(QwtPlot::yLeft,-1.0,1.0);

    new QwtPlotMagnifier(this->canvas());
    new QwtPlotZoomer(this->canvas());

    csin = new QwtPlotCurve("test sinx");
    csin->setRenderHint(QwtPlotItem::RenderAntialiased);
    csin->setLegendAttribute(QwtPlotCurve::LegendShowLine, true);
    csin->setPen(QPen(Qt::red));
    csin->attach(this);
}


#endif
