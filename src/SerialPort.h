#ifndef SERIALPORT_H
#define SERIALPORT_H

#include <QString>
#include <QObject>
#include <QIODevice>
#include <stdio.h>
#include <QDebug>
#include <QTimer>
#include <QMutex>

#ifdef Q_OS_WIN
#include <windows.h>
#endif

class SerialPortThread;

class QSerialPort : public QIODevice
{
    Q_OBJECT
public:
    explicit QSerialPort(void);
    ~QSerialPort(void);

    QMutex mutex;
    QByteArray receiveBuffer;

    bool open(QIODevice::OpenMode mode);
    void close();
    qint64 writeData(const char *txBuf, qint64 length);
    qint64 readData(char *rxBuf, qint64 length);
    qint64 bytesAvailable();

    void SetPortName(QString str);
    void SetBaudRate(QString str);
    void SetParity(QString str);
    void SetDataBits(QString str);
    void SetStopBits(QString str);
    void SetReadBufferSize(qint32 size);
    void SetWriteBufferSize(qint32 size);

    void MonitorEvent();
    void StopMonitor();

private:
#ifdef Q_OS_WIN
    ::DCB dcb;
    ::COMMTIMEOUTS ct;
    ::HANDLE hFile;
    ::OVERLAPPED oRead;
    ::OVERLAPPED oWrite;
    ::OVERLAPPED overlap;
#endif

    // windows:
    // portName: COM1,COM2
    // databits: Number of bits/byte, 4-8
    // parity: 0-4=None,Odd,Even,Mark,Space
    // stopbits: 0,1,2 = 1, 1.5, 2

    qint64 baudRate;
    qint8 stopBits;
    qint8 parity;
    qint8 dataBits;
    QString portName;
    qint32 readBufferSize;
    qint32 writeBufferSize;
    qint64 totalCount;
    char *rxBuf;
    QMutex portMutex;

    SerialPortThread *overlapThread;

    bool InitPort();
    bool ClearOverlapped(::OVERLAPPED *o);

Q_SIGNALS:
    void txEmpay();
    void rxChar();
    void portError(QString);
};

#include <QThread>
class SerialPortThread: public QThread
{
public:
    explicit SerialPortThread(QSerialPort *p);
    bool stoped;
    void stop();
protected:
    void run();
private:
    QSerialPort *port;
};

#endif  //SERIALPORT_H
