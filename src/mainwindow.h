#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QTimer>
#include <QDebug>
#include <QMutex>
#include <QThread>

#include <QtSerialPort/QSerialPortInfo>
#include <QtSerialPort/QtSerialPort>

#define _QWT_EN 1

#if _QWT_EN
#include <Qwt/qwt_plot.h>
#include <Qwt/qwt_plot_marker.h>
#include <Qwt/qwt_plot_curve.h>
#include <Qwt/qwt_legend.h>
#include <Qwt/qwt_series_data.h>
#include <Qwt/qwt_plot_canvas.h>
#include <Qwt/qwt_plot_panner.h>
#include <Qwt/qwt_plot_magnifier.h>
#include <Qwt/qwt_text.h>
#include <Qwt/qwt_math.h>
#include <Qwt/qwt_plot_zoomer.h>
#endif

#include "qxtcheckcombobox.h"


#define HISTORY 1000

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QSerialPort *port;
    QTimer *readTimer;
    QTimer *writeTimer;
    QTimer *enumerateTimer;
    QByteArray disArrayBuf;
    QString disString;
    qint64 dataCount;

    bool portIsOpened;
    bool isPaused;

#if _QWT_EN
    QwtPlot *qwtPlot;
    QwtPlotCurve *curve;
#endif
    QVector<QPointF> *pointBuf;
    qint32 sensorMaxDisCount;
    qint32 realSensorCount;
    QxtCheckComboBox *ccbSelectCurve;

    void RePlot();
    void InitPlotCanvas();
    void InitPlotCurves();
    void DelPlotCurves();

private Q_SLOTS:
    void UpdatePortName();
    void DisplayData();
    void readTimerStart();
    void ProcessPortError(QSerialPort::SerialPortError);
    void CheckedItemsChanged(QStringList l);

    void on_btnExit_clicked();
    void on_btnSwitch_clicked();
    void on_btnClear_clicked();
    void on_cbAutoWrite_stateChanged(int arg1);
    void on_btnWriteData_clicked();
    void on_btnPause_clicked();
    void on_tabWidget_currentChanged(int index);
    void on_sbSensorCount_valueChanged(int arg1);
    void on_cbAutoWrap_stateChanged(int arg1);
};

#endif // MAINWINDOW_H
