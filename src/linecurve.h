#ifdef LINECURVE_H
#define LINECURVE_H

#include "qwt_plot.h"
#include "qwt_plot_curve.h"
#include "qwt_plot_magnifier.h"
#include "qwt_plot_zoomer.h"

class LineCurve : public QwtPlot
{
public:
    explicit LineCurve( QWidget * = NULL );
    QwtPlotCurve *csin;
};

#endif // LINECURVE_H
