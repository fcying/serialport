#include "SerialPort.h"

QSerialPort::QSerialPort(void)
{
    baudRate = 115200;
    stopBits = 0;
    parity = 0;
    dataBits = 8;
    portName = "COM1";
    readBufferSize = 4096;
    writeBufferSize = 4096;
    totalCount = 0;
    receiveBuffer.clear();
    rxBuf = NULL;
    overlapThread = new SerialPortThread(this);
    oRead.hEvent = NULL;
    oWrite.hEvent = NULL;
    overlap.hEvent = NULL;
    hFile = NULL;
}

QSerialPort::~QSerialPort(void)
{
}

bool QSerialPort::open(QIODevice::OpenMode mode)
{
    QByteArray nativeFilePath = QByteArray(
                (const char *)portName.utf16(), portName.size()*2+1);
    ::DWORD access = 0;
    ::DWORD eventMask = 0;
    bool rx = false;
    bool tx = false;

    if (QIODevice::ReadOnly & mode)
    {
        access |= GENERIC_READ; //sharing = FILE_SHARE_READ;
        rx = true;
        eventMask |= EV_RXCHAR;
    }
    if (QIODevice::WriteOnly & mode)
    {
        access |= GENERIC_WRITE; //sharing = FILE_SHARE_WRITE;
        tx = true;
        eventMask |= EV_TXEMPTY;
    }

    hFile = ::CreateFile((const wchar_t*)nativeFilePath.constData(),
                         access,
                         0,
                         0,
                         OPEN_EXISTING,
                         FILE_FLAG_OVERLAPPED,
                         0);

    if(hFile == INVALID_HANDLE_VALUE)
        return false;

    //设置串口
    if(!InitPort())
    {
        close();
        return false;
    }

    //创建事件
    if (tx)
    {
        this->oWrite.hEvent = ::CreateEvent(0, true, false, 0);
    }
    if (rx)
    {
        this->oRead.hEvent = ::CreateEvent(0, true, false, 0);
    }
    overlap.hEvent = ::CreateEvent(0, true, false, 0);
    ClearOverlapped(&overlap);

    if (!::SetCommMask(hFile, eventMask))
    {
        close();
        return false;
    }

    //清除缓冲区
    if(!PurgeComm(hFile,PURGE_TXABORT|PURGE_RXABORT
                          |PURGE_TXCLEAR|PURGE_RXCLEAR))
    {
        close();
        return false ;
    }

    this->overlapThread->start();

    return true;
}

void QSerialPort::close()
{
    this->overlapThread->stop();
    if (rxBuf != NULL)
    {
        delete []rxBuf;
        rxBuf = NULL;
    }
    if (hFile != NULL)
    {
        ::CloseHandle(hFile);
        hFile = NULL;
    }
    if (oRead.hEvent != NULL)
    {
        ::CloseHandle(oRead.hEvent);
        oRead.hEvent = NULL;
    }
    if (oWrite.hEvent != NULL)
    {
        ::CloseHandle(oWrite.hEvent);
        oWrite.hEvent = NULL;
    }
    if (overlap.hEvent == NULL)
    {
        ::CloseHandle(overlap.hEvent);
        overlap.hEvent = NULL;
    }
}

bool QSerialPort::InitPort()
{
    //设置DCB
    if(!::GetCommState(hFile, &dcb)) //获取串口当前状况
    {
        return false;
    }

    dcb.BaudRate = this->baudRate;
    dcb.ByteSize = this->dataBits;
    dcb.StopBits = this->stopBits;
    dcb.Parity = this->parity;

    if(!::SetCommState(hFile, &dcb))
    {
        return false ;
    }

    //设置超时值
    ct.ReadIntervalTimeout = 100;
    ct.ReadTotalTimeoutMultiplier = 0;
    ct.ReadTotalTimeoutConstant = 1;
    ct.WriteTotalTimeoutMultiplier = 5000;
    ct.WriteTotalTimeoutConstant = 1;
    if(!::SetCommTimeouts(hFile, &ct))
    {
        return false;
    }

    //设置缓冲区大小
    if(!SetupComm(hFile, readBufferSize, writeBufferSize))
    {
        return false;
    }
    rxBuf = new char[readBufferSize];

    return true;
}

//写数据
qint64 QSerialPort::writeData(const char *txBuf, qint64 length)
{
    ::DWORD writeCount = 0;
    bool successResult = false;

    if ((length == -1) || (length > this->writeBufferSize))
    {
        length = this->writeBufferSize;
    }

    QMutexLocker(&this->portMutex);
    if (!ClearOverlapped(&this->oWrite))
    {
        return qint64(-1);
    }

    if (::WriteFile(hFile, txBuf, length, &writeCount, &oWrite))
    {
        successResult = true;
    }
    else
    {
        ::DWORD error = ::GetLastError();
        if (error == ERROR_IO_PENDING)
        {
            error = ::WaitForSingleObject(oWrite.hEvent, 5000);
            switch (error)
            {
                case WAIT_OBJECT_0:
                    if (::GetOverlappedResult(hFile, &oWrite, &writeCount, false))
                        successResult = true;
                    else
                        successResult = false;
                    break;
                default:
                    successResult = false;
                    break;
            }
        }
    }

    if(writeCount != length)
        successResult = false;

    return  successResult ? qint64(writeCount) : qint64(-1);
}

//读数据
qint64 QSerialPort::readData(char *rxBuf, qint64 length)
{
    ::DWORD readCount = 0;
    bool successResult = false;

    if ((length == -1) || (length > this->readBufferSize))
    {
        length = this->readBufferSize;
    }

    QMutexLocker(&this->portMutex);
    if (!ClearOverlapped(&this->oRead))
    {
        return qint64(-1);
    }

    if(ReadFile(hFile, rxBuf, length, &readCount, &oRead))
    {
        successResult = true;
    }
    else
    {
        ::DWORD error = ::GetLastError();
        if (error == ERROR_IO_PENDING)
        {
            error = ::WaitForSingleObject(oRead.hEvent, 5000);
            switch (error)
            {
                case WAIT_OBJECT_0:
                    if (::GetOverlappedResult(hFile, &oRead, &readCount, false))
                        successResult = true;
                    else
                        successResult = false;
                    break;
                default:
                    successResult = false;
                    break;
            }
        }
    }

    return  successResult ? qint64(readCount) : qint64(-1);
}

//返回缓存内数据总数
qint64 QSerialPort::bytesAvailable()
{
    DWORD commErrors = 0;
    COMSTAT commStat;
    QMutexLocker(&this->portMutex);
    ::ClearCommError(hFile, &commErrors, &commStat);
    return commStat.cbInQue;
}

//串口事件监听
void QSerialPort::MonitorEvent()
{
    ::DWORD eventMask = 0;
    ::DWORD error = 0;

    ResetEvent(overlap.hEvent);
    if (!::WaitCommEvent(hFile, &eventMask, &overlap))
    {
        error = ::GetLastError();
        if (error != ERROR_IO_PENDING)
        {
            this->StopMonitor();
            emit portError("WaitCommEvent error"+QString::number(error));
            qCritical("WaitCommEvent error %ld\n", error);
            return;
        }
    }

    error = ::WaitForSingleObject(overlap.hEvent, INFINITE);
    if (error == WAIT_OBJECT_0)
    {
        if (eventMask & EV_RXCHAR)
        {
            qint64 length;
            length = this->readData(rxBuf, -1);
            QMutexLocker locker(&mutex);
            receiveBuffer.append(rxBuf, length);
            if (receiveBuffer.length() > readBufferSize)
                receiveBuffer.remove(0, receiveBuffer.length()-readBufferSize);
            locker.unlock();
            emit rxChar();
        }
        if (eventMask & EV_TXEMPTY)
        {
            emit txEmpay();
        }
    }
}

//停止串口事件监听
void QSerialPort::StopMonitor()
{
    overlapThread->stoped = true;
    ::SetCommMask(hFile, 0);
}

void QSerialPort::SetPortName(QString str)
{
    this->portName = "\\\\.\\" + str;
}

void QSerialPort::SetBaudRate(QString str)
{
    this->baudRate = str.toInt();
}

void QSerialPort::SetDataBits(QString str)
{
    this->dataBits = str.toInt();
}

void QSerialPort::SetParity(QString str)
{
    if (str.compare("None") == 0)
        this->parity = 0;
    else if (str.compare("Odd") == 0)
        this->parity = 1;
    else if (str.compare("Even") == 0)
        this->parity = 2;
    else if (str.compare("Mark") == 0)
        this->parity = 3;
    else if (str.compare("Space") == 0)
        this->parity = 4;
}

void QSerialPort::SetStopBits(QString str)
{
    if (str.compare("1") == 0)
        this->stopBits = 0;
    else if (str.compare("1.5") == 0)
        this->stopBits = 1;
    else if (str.compare("2") == 0)
        this->stopBits = 2;
}

void QSerialPort::SetReadBufferSize(qint32 size)
{
    this->readBufferSize = size;
}

void QSerialPort::SetWriteBufferSize(qint32 size)
{
    this->writeBufferSize = size;
}

bool QSerialPort::ClearOverlapped(::OVERLAPPED *o)
{
    o->Internal = o->InternalHigh = o->Offset = o->OffsetHigh = 0;
    ::ResetEvent(o->hEvent);
    return (0 != o->hEvent);
}


/*
  事件监听线程
*/
SerialPortThread::SerialPortThread(QSerialPort *p)
{
    this->port = p;
    this->stoped = true;
}

void SerialPortThread::run()
{
    this->stoped = false;
    while (!this->stoped)
    {
        this->port->MonitorEvent();
    }
}

void SerialPortThread::stop()
{
    if (this->isFinished())
        return;
    this->port->StopMonitor();
    while(this->isRunning());
}



